var express = require('express'); // Web Framework
var sql = require('mssql'); // MS Sql Server client
//const Vision6 = require('vision6-jsonrpc-client');

var cron = require('node-cron');

const fs = require("fs");
var app = express();
var Slack = require('slack-node');

webhookUri = "https://hooks.slack.com/services/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";
 
var slack = new Slack();
slack.setWebhook(webhookUri);

// mins | hours | day of month | month | day of week
// i.e. run after AppDates population currently 8:10, 12:10, 4:10
cron.schedule('30 8,12,4,5 * * *', () => {
  console.log('Run at 8,12 and 4 to populate Company Performance for conversion data');
  PerformanceData();
  //sendMail('4025 synced');
});

async function execute2(query) {

    return new Promise((resolve, reject) => {

        new sql.ConnectionPool(sqlConfigBCReplica).connect().then(pool => {
            return pool.request().query(query)
        }).then(result => {

            resolve(result.recordset[0].CompanyPerformancestatus);
			
			console.log(result.recordset[0].CompanyPerformancestatus);

            sql.close();
        }).catch(err => {

            reject(err)
            sql.close();
        });
    });
};


		  // Connection string parameters.
var sqlConfigBCReplica = {
    user: 'sa',
    password: 'R3p0rt1ng!',
    server: 'localhost',
    database: 'BetterChoice_Central',
	requestTimeout: 300000
}


function sendMail(message) {
			    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Company Performance Data update 👻" <chris.m@betterchoice.com.au>', // sender address
        to: 'chris.m@betterchoice.com.au', // list of receivers
        subject: 'Company Performance Data update ✔', // Subject line
        text: 'Company Performance Data update: ' + message, // plain text body
        html: '<b>Company Performance Data update: ' + message + '</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    });
}
function sendSlack(message) {
	slack.webhook({
	  channel: "#node",
	  username: "nodebot",
	  icon_emoji: ":ghost:",
	  text: 'Company Performance Data update: ' + message
	}, function(err, response) {
	  console.log(response);
	});
}
var errHandler = function(err) {
    console.log(err);
	//sendMail(err);
	sendSlack(err);
}

function PerformanceData() {
		var promise = execute2('exec [dbo].[bc_Populate_NBByData_All]');
		
		promise.then(function (results) {
			sendSlack(results);
		}, function (err) {
			errHandler(err) 
		})
}

let nodemailer = require("nodemailer");

// create mail transporter
    let transporter = nodemailer.createTransport({
        host: '10.51.0.79',
        port: 25,
        secure: false, // true for 465, false for other ports
        /*auth: {
            user: account.user, // generated ethereal user
            pass: account.pass // generated ethereal password
        }*/
    });


app.listen(3149);

// Start server and listen on http://localhost:8082/
/*var server = app.listen(8082, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("app listening at http://%s:%s", host, port)
});*/

